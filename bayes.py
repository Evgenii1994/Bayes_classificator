import copy

class Bayes:
    """

    Returns appropriate data for using this implementation
    @notice returned dataset doesn't contain objects with '?' features
    @param filename the name of the file to handle

    """
    def handle_data(self, filename):
        data = list()
        file = open(filename, 'r')

        for line in file:
            string = ''.join(line.split())
            data.append(string.split(','))

        correct_data = list()
        for element in data:
            indicator = 0
            for word in element:
                if word == '?':
                    indicator = 1
            if indicator == 0:
                correct_data.append(element)
        return correct_data

    """

    Calculates probabilities of each class in the dataset
    @param train_data training data

    """
    def normalized_number_in_classes(self, train_data):
        dictionary_of_classes = dict()
        for line in train_data:
            if line[-1] not in dictionary_of_classes:
                dictionary_of_classes[line[-1]] = 1
            else:
                dictionary_of_classes[line[-1]] += 1

        in_total = 0
        for _class in dictionary_of_classes:
            in_total += dictionary_of_classes[_class]
        for _class in dictionary_of_classes:
            dictionary_of_classes[_class] /= in_total
        self.classes = dictionary_of_classes

    """

    Calculates probabilities for each kind of feature in each class
    @param train_data training data

    """
    def normalized_number_in_classes_for_each_feature(self, train_data):
        dict_of_instances = dict()
        for instance in train_data:
            if instance[-1] not in dict_of_instances:
                dict_of_instances[instance[-1]] = dict()

            for feature in range(len(instance) - 1):
                if feature not in dict_of_instances[instance[-1]]:
                    dict_of_instances[instance[-1]][feature] = dict()
                if instance[feature] not in dict_of_instances[instance[-1]][feature]:
                    dict_of_instances[instance[-1]][feature][instance[feature]] = 1
                else:
                    dict_of_instances[instance[-1]][feature][instance[feature]] += 1

        for _class in dict_of_instances:
            for feature in dict_of_instances[_class]:
                in_total = 0
                for kind_of_feature in dict_of_instances[_class][feature]:
                    in_total += dict_of_instances[_class][feature][kind_of_feature]
                for kind_of_feature in dict_of_instances[_class][feature]:
                    dict_of_instances[_class][feature][kind_of_feature] /= in_total

        self.dict_of_instances = dict_of_instances

    """

    Bayes algorithm
    @param train_data training data
    @param test_data testing data

    """
    def process_Bayes(self, train_data, test_data_):
        self.normalized_number_in_classes(train_data)
        self.normalized_number_in_classes_for_each_feature(train_data)

        classifyied_data = list()
        test_data = copy.deepcopy(test_data_)
        for instance in test_data:
            defined_class = -1
            max = 0
            for _class in self.dict_of_instances:
                value = self.classes[_class]
                for feature in range(len(instance) - 1):
                    if (instance[feature] in
                        self.dict_of_instances[_class][feature]):
                        value *= self.dict_of_instances[_class][feature][instance[feature]]
                    else:
                        value = 0
                if max <= value:
                    max = value
                    defined_class = _class

            instance[-1] = defined_class
            classifyied_data.append(instance)
        return classifyied_data

    """

    Calculates a standart accuracy
    @param test_data testing data
    @param classifyied_data the result of process_Bayes function

    """
    def accuracy(self, test_data, classifyied_data):
        correct_in_total = 0
        for i in range(len(test_data)):
            if test_data[i] == classifyied_data[i]:
                correct_in_total += 1
        return correct_in_total / len(test_data)

def main():
    session1 = Bayes()
    train_data = session1.handle_data('golf_train.txt')
    test_data = session1.handle_data('golf_test.txt')
    classifyied_data = session1.process_Bayes(train_data, test_data)
    print('Accuracy: ', session1.accuracy(test_data, classifyied_data))

    session2 = Bayes()
    train_data = session2.handle_data('soybean_train.txt')
    test_data = session2.handle_data('soybean_test.txt')
    classifyied_data = session2.process_Bayes(train_data, test_data)
    print('Accuracy: ', session2.accuracy(test_data, classifyied_data))



if __name__ == "__main__":
    main()
